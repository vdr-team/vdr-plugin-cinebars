vdr-plugin-cinebars (0.0.5-7) experimental; urgency=low

  * Removed non-standard shebang line from debian/rules
  * Standards-Version: 3.8.3

 -- Tobias Grimm <etobi@debian.org>  Mon, 09 Nov 2009 20:50:17 +0100

vdr-plugin-cinebars (0.0.5-6) experimental; urgency=low

  * Release for vdrdevel 1.7.6
  * Added ${misc:Depends}
  * Bumped standards version to 3.8.1
  * Changed section to "video"

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 29 Apr 2009 23:11:17 +0200

vdr-plugin-cinebars (0.0.5-5) experimental; urgency=low

  * Dropped patchlevel control field
  * Build-Depend on vdr-dev (>=1.6.0-5)
  * Bumped Standards-Version to 3.8.0
  * Using COMPAT=5 now

 -- Tobias Grimm <tg@e-tobi.net>  Fri, 25 Jul 2008 15:52:51 +0200

vdr-plugin-cinebars (0.0.5-4) experimental; urgency=low

  * Increased package version to force rebuild for vdr 1.6.0-1ctvdr7

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 12 May 2008 00:55:46 +0200

vdr-plugin-cinebars (0.0.5-3) experimental; urgency=low

  * Build-Depend on vdr-dev (>= 1.6.0)

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 24 Mar 2008 20:14:05 +0100

vdr-plugin-cinebars (0.0.5-2) experimental; urgency=low

  * Force rebuild for vdr 1.5.15

 -- Tobias Grimm <tg@e-tobi.net>  Mon, 18 Feb 2008 21:32:25 +0100

vdr-plugin-cinebars (0.0.5-1) unstable; urgency=low

  * New upstream release
  * Removed 01_Makefile-fPIC-fix.dpatch (fixed upstream)
  * Removed 90_cinebars-0.0.4-trNOOP.dpatch (fixed upstream)
  * Switched Build-System to cdbs, Build-Depend on cdbs
  * Added Homepage field to debian/control
  * Bumped Standards-Version to 3.7.3
  * Removed old homepage (http://home.pages.at/brougs78) from debian/copyright
  * Updated copyright year

 -- Tobias Grimm <tg@e-tobi.net>  Sat, 26 Jan 2008 12:58:45 +0100

vdr-plugin-cinebars (0.0.4-6) unstable; urgency=low

  * Release for vdrdevel 1.5.13

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 16 Jan 2008 10:31:44 +0100

vdr-plugin-cinebars (0.0.4-5) unstable; urgency=low

  * Release for vdrdevel 1.5.12

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 20 Nov 2007 23:46:37 +0100

vdr-plugin-cinebars (0.0.4-4) unstable; urgency=low

  * Release for vdrdevel 1.5.11

 -- Thomas Günther <tom@toms-cafe.de>  Tue,  6 Nov 2007 23:34:29 +0100

vdr-plugin-cinebars (0.0.4-3) unstable; urgency=low

  * Release for vdrdevel 1.5.10

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 16 Oct 2007 23:51:08 +0200

vdr-plugin-cinebars (0.0.4-2) unstable; urgency=low

  [ Tobias Grimm ]
  * Uwe Hanke has taken over the upstream development

  [ Thomas Günther ]
  * Added gettext to Build-Depends
  * Added installation of locale files (if VDR >= 1.5.7)
  * Added 90_cinebars-0.0.4-trNOOP.dpatch

 -- Thomas Günther <tom@toms-cafe.de>  Fri, 28 Sep 2007 23:58:41 +0200

vdr-plugin-cinebars (0.0.4-1) unstable; urgency=low

  * New upstream release
  * Removed 90_APIVERSION.dpatch - fixed upstream
  * Updated debian/copyright
  * Fixed debian-rules-ignores-make-clean-error

 -- Tobias Grimm <tg@e-tobi.net>  Thu, 27 Sep 2007 21:10:18 +0200

vdr-plugin-cinebars (0.0.3a-16) unstable; urgency=low

  * Release for vdrdevel 1.5.9

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 28 Aug 2007 01:01:19 +0200

vdr-plugin-cinebars (0.0.3a-15) unstable; urgency=low

  * Release for vdrdevel 1.5.8

 -- Thomas Günther <tom@toms-cafe.de>  Thu, 23 Aug 2007 01:09:18 +0200

vdr-plugin-cinebars (0.0.3a-14) unstable; urgency=low

  * Release for vdrdevel 1.5.6

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 14 Aug 2007 01:46:30 +0200

vdr-plugin-cinebars (0.0.3a-13) unstable; urgency=low

  * Release for vdrdevel 1.5.5

 -- Thomas Günther <tom@toms-cafe.de>  Wed, 27 Jun 2007 23:02:54 +0200

vdr-plugin-cinebars (0.0.3a-12) unstable; urgency=low

  * Release for vdrdevel 1.5.2

 -- Thomas Günther <tom@toms-cafe.de>  Sat, 28 Apr 2007 00:00:58 +0200

vdr-plugin-cinebars (0.0.3a-11) unstable; urgency=low

  * Release for vdrdevel 1.5.1

 -- Thomas Günther <tom@toms-cafe.de>  Tue, 27 Feb 2007 19:59:28 +0100

vdr-plugin-cinebars (0.0.3a-10) unstable; urgency=low

  [ Thomas Günther ]
  * Replaced VDRdevel adaptions in debian/rules with make-special-vdr
  * Adapted call of dependencies.sh and patchlevel.sh to the new location
    in /usr/share/vdr-dev/

  [ Tobias Grimm ]
  * Build-Depend on vdr-dev (>=1.4.5-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sun, 14 Jan 2007 19:12:19 +0100

vdr-plugin-cinebars (0.0.3a-9) unstable; urgency=low

  * Build-Depend on vdr-dev (>=1.4.4-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sun,  5 Nov 2006 11:45:37 +0100

vdr-plugin-cinebars (0.0.3a-8) unstable; urgency=low

  * Build-Depend on vdr-dev (>=1.4.3-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sat, 23 Sep 2006 20:32:53 +0200

vdr-plugin-cinebars (0.0.3a-7) unstable; urgency=low

  * Build-Depend on vdr-dev (>=1.4.2-1)
  * Bumped Standards-Version to 3.7.2

 -- Tobias Grimm <tg@e-tobi.net>  Sun, 27 Aug 2006 14:39:28 +0200

vdr-plugin-cinebars (0.0.3a-6) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.4.1-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Fri, 16 Jun 2006 21:27:33 +0200

vdr-plugin-cinebars (0.0.3a-5) unstable; urgency=low

  * Thomas Schmidt <tschmidt@debian.org>
    - Build-Depend on vdr-dev (>=1.4.0-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Wed,  3 May 2006 23:05:55 +0200

vdr-plugin-cinebars (0.0.3a-4) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Build-Depend on vdr (>=1.3.48-1)
    - Added 90_APIVERSION.dpatch

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Mon, 24 Apr 2006 22:08:30 +0200

vdr-plugin-cinebars (0.0.3a-3) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Build-Depend on vdr (>=1.3.46-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Wed, 12 Apr 2006 23:36:46 +0200

vdr-plugin-cinebars (0.0.3a-2) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Build-Depend on vdr (>=1.3.45-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Mon, 27 Mar 2006 21:08:03 +0200

vdr-plugin-cinebars (0.0.3a-1) unstable; urgency=low

  * Thomas Günther <tom@toms-cafe.de>
    - New upstream release
    - Added debian/watch file for uscan

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Thu, 23 Mar 2006 21:06:25 +0100

vdr-plugin-cinebars (0.0.2-5) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Build-Depend on vdr (>=1.3.41-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Tue, 31 Jan 2006 01:40:47 +0100

vdr-plugin-cinebars (0.0.2-4) unstable; urgency=low

  * Tobias Grimm <tg@e-tobi.net>
    - Build-Depend on vdr (>=1.3.40-1)

 -- Debian VDR Team <pkg-vdr-dvb-devel@lists.alioth.debian.org>  Wed, 25 Jan 2006 23:36:29 +0100

vdr-plugin-cinebars (0.0.2-3) unstable; urgency=low

  * Build-Depend on vdr (>=1.3.37-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sat,  3 Dec 2005 20:56:23 +0100

vdr-plugin-cinebars (0.0.2-2) unstable; urgency=low

  * Build-Depend on vdr (>=1.3.33-1)

 -- Tobias Grimm <tg@e-tobi.net>  Sun,  2 Oct 2005 11:32:36 +0200

vdr-plugin-cinebars (0.0.2-1) unstable; urgency=low

  * Initial Release.

 -- Michael Mauksch <mysterix@mandaxy.de>  Tue, 20 Sep 2005 19:43:50 +0200

